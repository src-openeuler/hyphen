Name:      hyphen
Version:   2.8.8
Release:   16
Summary:   A library of text hyphenation
Group:     System Environment/Libraries
License:   GPLv2 or LGPLv2+ or MPLv1.1
URL:       http://hunspell.github.io
Source0:   http://downloads.sourceforge.net/hunspell/%{name}-%{version}.tar.gz

BuildRequires: perl-interpreter, patch, autoconf, automake, libtool
Provides:  %{name}-en

%description
Hyphen is a library for high quality text hyphenation and justification.

%package   devel
Summary:   Files for %{name} development
Requires:  %{name} = %{version}-%{release}
Group:     Development/Libraries

%description devel
Includes and definitions for developing with hyphen

%prep
%autosetup

%build
%configure --disable-static
%make_build

%install
%make_install
rm -f %{buildroot}/%{_libdir}/*.la

cd %{buildroot}/%{_datadir}/%{name}/
en_US_aliases="en_AG en_AU en_BS en_BW en_BZ en_CA en_DK en_GB en_GH en_HK en_IE en_IN en_JM en_MW en_NA en_NZ en_PH en_SG en_TT en_ZA en_ZM en_ZW"
for lang in $en_US_aliases; do
        ln -s hyph_en_US.dic hyph_$lang.dic
done
cd ..

%check
make check

%ldconfig_scriptlets

%postun -p /sbin/ldconfig

%files
%doc ChangeLog README README.%{name} README.nonstandard TODO
%license AUTHORS COPYING*
%{_libdir}/*.so.*
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/hyph_en*.dic

%files devel
%{_includedir}/%{name}.h
%{_libdir}/*.so
%{_bindir}/substrings.pl

%changelog
* Wed Sep 06 2023 Darssin <2020303249@mail.nwpu.edu.cn> - 2.8.8-16
- Type: bugfix
- ID: NA
- SUG: NA
- DESC: add Provides:  hyphen-en

* Tue Oct 25 2022 yanglongkang<yanglongkang@h-partners.com> - 2.8.8-15
- Type: bugfix
- ID: NA
- SUG: NA
- DESC: rebuild for next release

* Tue Jun 30 2020 chenditang <chenditang1@huawei.com> - 2.8.8-14
- Type:NA
- ID:NA
- SUG:NA
- DESC:delete valgrind make check

* Mon Dec 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.8.8-13
- Type:NA
- ID:NA
- SUG:NA
- DESC:modify email adress

* Fri Oct 18 2019 fangyufa <fangyufa1@huawei.com> - 2.8.8-12
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix spec in openEuler rule

* Fri Sep 06 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.8.8-11
- Package init
